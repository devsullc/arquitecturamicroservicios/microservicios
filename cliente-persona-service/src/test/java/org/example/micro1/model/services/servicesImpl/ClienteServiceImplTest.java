package org.example.micro1.model.services.servicesImpl;

import org.example.micro1.config.AppConfig;
import org.example.micro1.model.dto.ClienteDTO;
import org.example.micro1.model.entitys.Cliente;
import org.example.micro1.model.repositorys.ClienteRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.modelmapper.ModelMapper;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.ContextConfiguration;

import static org.mockito.ArgumentMatchers.any;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@ContextConfiguration(classes = {AppConfig.class, ClienteServiceImpl.class})
class ClienteServiceImplTest {
    @Mock
    private ClienteRepository clienteRepository;

    @InjectMocks
    private ClienteServiceImpl clienteServiceImpl;
    @Mock
    private ModelMapper modelMapper;

    @Mock
    private KafkaTemplate<String, ClienteDTO> kafkaTemplate;
    @Test
    public void test_creates_new_cliente_entity() {

        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setId(1L);
        clienteDTO.setNombre("John Doe");
        clienteDTO.setGenero("Male");
        clienteDTO.setEdad(30);
        clienteDTO.setIdentificacion("123456789");
        clienteDTO.setDireccion("123 Main St");
        clienteDTO.setTelefono("555-1234");
        clienteDTO.setClienteId("12345");
        clienteDTO.setContrasena("password");
        clienteDTO.setEstado(true);

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        Mockito.when(modelMapper.map(clienteDTO, Cliente.class)).thenReturn(cliente);
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);
        Mockito.when(modelMapper.map(cliente, ClienteDTO.class)).thenReturn(clienteDTO);

        ClienteDTO result = clienteServiceImpl.crearCliente(clienteDTO);

        assertNotNull(result);
        assertEquals(clienteDTO.getNombre(), result.getNombre());
        assertEquals(clienteDTO.getGenero(), result.getGenero());
        assertEquals(clienteDTO.getEdad(), result.getEdad());
        assertEquals(clienteDTO.getIdentificacion(), result.getIdentificacion());
        assertEquals(clienteDTO.getDireccion(), result.getDireccion());
        assertEquals(clienteDTO.getTelefono(), result.getTelefono());
        assertEquals(clienteDTO.getClienteId(), result.getClienteId());
        assertEquals(clienteDTO.getContrasena(), result.getContrasena());
        assertEquals(clienteDTO.getEstado(), result.getEstado());

        Mockito.verify(kafkaTemplate).send("cliente-creado", cliente.getId().toString(), clienteDTO);
    }
    @Test
    public void test_saves_new_cliente_entity_to_database() {

        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setId(1L);
        clienteDTO.setNombre("John Doe");
        clienteDTO.setGenero("Male");
        clienteDTO.setEdad(30);
        clienteDTO.setIdentificacion("123456789");
        clienteDTO.setDireccion("123 Main St");
        clienteDTO.setTelefono("555-1234");
        clienteDTO.setClienteId("12345");
        clienteDTO.setContrasena("password");
        clienteDTO.setEstado(true);

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        Mockito.when(modelMapper.map(clienteDTO, Cliente.class)).thenReturn(cliente);
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);

        ClienteDTO result = clienteServiceImpl.crearCliente(clienteDTO);


        Mockito.verify(clienteRepository, Mockito.times(1)).save(cliente);
    }
    @Test
    public void test_null_attributes() {

        ClienteDTO clienteDTO = new ClienteDTO();

        Cliente cliente = new Cliente();
        Mockito.when(modelMapper.map(clienteDTO, Cliente.class)).thenReturn(cliente);
        Mockito.when(clienteRepository.save(cliente)).thenAnswer(invocation -> {
            Cliente savedCliente = invocation.getArgument(0);
            savedCliente.setId(1L);
            return savedCliente;
        });
        Mockito.when(modelMapper.map(cliente, ClienteDTO.class)).thenReturn(clienteDTO);

        ClienteDTO result = clienteServiceImpl.crearCliente(clienteDTO);

        assertNotNull(result);
        assertNull(result.getNombre());
        assertNull(result.getGenero());
        assertNull(result.getEdad());
        assertNull(result.getIdentificacion());
        assertNull(result.getDireccion());
        assertNull(result.getTelefono());
        assertNull(result.getClienteId());
        assertNull(result.getContrasena());
        assertNull(result.getEstado());
    }


    @Test
    public void test_throws_null_pointer_exception_for_null_contrasena() {

        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setId(1L);
        clienteDTO.setNombre("John Doe");
        clienteDTO.setGenero("Male");
        clienteDTO.setEdad(30);
        clienteDTO.setIdentificacion("123456789");
        clienteDTO.setDireccion("123 Main St");
        clienteDTO.setTelefono("555-1234");
        clienteDTO.setClienteId("12345");
        clienteDTO.setContrasena(null);
        clienteDTO.setEstado(true);

        assertThrows(NullPointerException.class, () -> {
            clienteServiceImpl.crearCliente(clienteDTO);
        });
    }
    @Test
    public void test_throws_null_pointer_exception_for_null_cliente_id() {

        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setId(1L);
        clienteDTO.setNombre("John Doe");
        clienteDTO.setGenero("Male");
        clienteDTO.setEdad(30);
        clienteDTO.setIdentificacion("123456789");
        clienteDTO.setDireccion("123 Main St");
        clienteDTO.setTelefono("555-1234");
        clienteDTO.setClienteId(null);
        clienteDTO.setContrasena("password");
        clienteDTO.setEstado(true);


        assertThrows(NullPointerException.class, () -> {
            clienteServiceImpl.crearCliente(clienteDTO);
        });
    }

    @Test
    public void test_null_clienteId() {

        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setNombre("John Doe");
        clienteDTO.setGenero("Male");
        clienteDTO.setEdad(30);
        clienteDTO.setIdentificacion("123456789");
        clienteDTO.setDireccion("123 Main St");
        clienteDTO.setTelefono("555-1234");
        clienteDTO.setClienteId(null);
        clienteDTO.setContrasena("password");
        clienteDTO.setEstado(true);

        assertThrows(NullPointerException.class, () -> {
            clienteServiceImpl.crearCliente(clienteDTO);
        });
    }
    @Test
    public void test_null_contrasena() {

        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setContrasena(null);

        assertThrows(NullPointerException.class, () -> {
            clienteServiceImpl.crearCliente(clienteDTO);
        });
    }
}