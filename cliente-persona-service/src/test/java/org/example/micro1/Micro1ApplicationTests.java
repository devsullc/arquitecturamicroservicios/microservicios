package org.example.micro1;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class Micro1ApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testCreateCliente() throws Exception {
        String jsonCliente = "{\"nombre\":\"John Doe\",\"genero\":\"Male\",\"edad\":30,\"identificacion\":\"123456789\",\"direccion\":\"123 Main St\",\"telefono\":\"555-1234\",\"clienteId\":\"12345\",\"contrasena\":\"password\",\"estado\":true}";

        mockMvc.perform(MockMvcRequestBuilders.post("/clientes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonCliente))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nombre").value("John Doe"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.identificacion").value("123456789"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.estado").value(true));
    }
}
