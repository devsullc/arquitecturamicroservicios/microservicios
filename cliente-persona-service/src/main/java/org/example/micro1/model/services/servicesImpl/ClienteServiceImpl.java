package org.example.micro1.model.services.servicesImpl;

import org.example.micro1.constantes.Constantes;
import org.example.micro1.model.dto.ClienteDTO;
import org.example.micro1.model.entitys.Cliente;
import org.example.micro1.model.repositorys.ClienteRepository;
import org.example.micro1.model.services.ClienteService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ClienteServiceImpl implements ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    @Lazy
    private KafkaTemplate<String, ClienteDTO> kafkaTemplate;

    @Override
    public ClienteDTO crearCliente(ClienteDTO clienteDTO) {
        Cliente cliente = modelMapper.map(clienteDTO, Cliente.class);
        cliente = clienteRepository.save(cliente);
        Map<String, Object> clienteDTOMap = new HashMap<>();
        clienteDTOMap.put(Constantes.CLIENTEID, cliente.getClienteId());
        clienteDTOMap.put(Constantes.NOMBRE, cliente.getNombre());
        clienteDTOMap.put(Constantes.GENERO, cliente.getGenero());
        clienteDTOMap.put(Constantes.EDAD, cliente.getEdad());
        clienteDTOMap.put(Constantes.IDENTIFICACION, cliente.getIdentificacion());
        clienteDTOMap.put(Constantes.DIRECCION, cliente.getDireccion());
        clienteDTOMap.put(Constantes.TELEFONO, cliente.getTelefono());
        clienteDTOMap.put(Constantes.CONTRASENA, cliente.getContrasena());
        clienteDTOMap.put(Constantes.ESTADO, cliente.isEstado());
        if (cliente.getId() != null) {
            if (kafkaTemplate == null) {
                throw new IllegalStateException(Constantes.ERROR_KAFKA_TEMPLATE);
            }else {
                kafkaTemplate.send(Constantes.TOPIC_CLIENTE_CREADO, cliente.getId().toString(), clienteDTO);

            }
        } else {
            throw new IllegalStateException(Constantes.ERROR_ID_CLIENTE);
        }
        return modelMapper.map(cliente, ClienteDTO.class);
    }
    @Override
    public ClienteDTO obtenerClientePorId(Long id) {
        Cliente cliente = clienteRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(Constantes.ERROR_CLIENTE_NO_ENCOTRADO));
        return modelMapper.map(cliente, ClienteDTO.class);
    }

    @Override
    public ClienteDTO actualizarCliente(Long id, ClienteDTO clienteDTO) {
        Cliente clienteExistente = clienteRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(Constantes.ERROR_CLIENTE_NO_ENCOTRADO));
        clienteExistente.setNombre(clienteDTO.getNombre());
        clienteExistente.setGenero(clienteDTO.getGenero());
        clienteExistente.setEdad(clienteDTO.getEdad());
        clienteExistente.setIdentificacion(clienteDTO.getIdentificacion());
        clienteExistente.setDireccion(clienteDTO.getDireccion());
        clienteExistente.setTelefono(clienteDTO.getTelefono());
        clienteExistente.setClienteId(clienteDTO.getClienteId());
        clienteExistente.setContrasena(clienteDTO.getContrasena());
        clienteExistente.setEstado(clienteDTO.getEstado());
        Cliente actualizado = clienteRepository.save(clienteExistente);
        Map<String, Object> clienteDTOMap = new HashMap<>();
        clienteDTOMap.put(Constantes.CLIENTEID, actualizado.getClienteId());
        clienteDTOMap.put(Constantes.NOMBRE ,actualizado.getNombre());
        clienteDTOMap.put(Constantes.GENERO ,actualizado.getGenero());
        clienteDTOMap.put(Constantes.EDAD ,actualizado.getEdad());
        clienteDTOMap.put(Constantes.IDENTIFICACION ,actualizado.getIdentificacion());
        clienteDTOMap.put(Constantes.DIRECCION ,actualizado.getDireccion());
        clienteDTOMap.put(Constantes.TELEFONO ,actualizado.getTelefono());
        clienteDTOMap.put(Constantes.CONTRASENA ,actualizado.getContrasena());
        clienteDTOMap.put(Constantes.ESTADO ,actualizado.isEstado());
        kafkaTemplate.send(Constantes.TOPIC_CLIENTE_ACTUALIZADO, actualizado.getId().toString(), clienteDTO);
        return modelMapper.map(actualizado, ClienteDTO.class);
    }

    @Override
    public void eliminarCliente(Long id) {
        if (!clienteRepository.existsById(id)) {
            throw new RuntimeException(Constantes.ERROR_CLIENTE_NO_ENCOTRADO);
        }
        clienteRepository.deleteById(id);
    }
}
