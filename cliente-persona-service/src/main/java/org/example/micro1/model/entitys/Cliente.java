package org.example.micro1.model.entitys;

import jakarta.persistence.Entity;
import lombok.Data;

@Data
@Entity
public class Cliente extends Persona {
    private String clienteId;
    private String contrasena;
    private boolean estado;
}
