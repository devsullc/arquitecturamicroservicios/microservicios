package org.example.micro1.constantes;

public class Constantes {

    public final static String HOST_KAFKA = "localhost:9092";

    //ERRORS
    public final static String ERROR_HANDLE_EXCEPTION = "Ocurrió un error en el servidor: ";
    public final static String ERROR_KAFKA_TEMPLATE = "KafkaTemplate no ha sido inyectado correctamente.";
    public final static String ERROR_ID_CLIENTE = "El ID del cliente no se generó correctamente.";
    public final static String ERROR_CLIENTE_NO_ENCOTRADO = "Cliente no encontrado";

    //TOPICS
    public final static String TOPIC_CLIENTE_CREADO = "cliente-creado";
    public final static String TOPIC_CLIENTE_ACTUALIZADO = "cliente-actualizado";

    //MAP KEY CLIENTE
    public final static String CLIENTEID = "clienteId";
    public final static String NOMBRE = "nombre";
    public final static String GENERO = "genero";
    public final static String EDAD = "edad";
    public final static String IDENTIFICACION = "identificacion";
    public final static String DIRECCION = "direccion";
    public final static String TELEFONO = "telefono";
    public final static String CONTRASENA = "contrasena";
    public final static String ESTADO = "estado";






}
