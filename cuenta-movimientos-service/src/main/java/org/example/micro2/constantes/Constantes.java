package org.example.micro2.constantes;

public class Constantes {

    public final static String HOST_KAFKA = "localhost:9092";

    //ERRORS
    public final static String ERROR_HANDLE_EXCEPTION = "Ocurrió un error en el servidor: ";
    public final static String ERROR_CLIENTE_NO_ENCOTRADO = "Cliente no encontrado";
    public final static String ERROR_CUENTA_NO_ENCOTRADO = "Cuenta no encontrado";
    public final static String ERROR_MOVIMIENTOS_NO_ENCOTRADO = "Movimiento no encontrado con ID: ";
    public final static String ERROR_SERVER = "Error interno del servidor";
    public final static String ERROR_FECHAS = "La fecha de fin no puede ser anterior a la fecha de inicio.";
    public final static String ERROR_FECHAS_CLIENTE = "Parámetros de fecha o cliente no pueden ser nulos.";
    public final static String ERROR_PROCESAR_MENSAJE = "Error al procesar el mensaje: ";

    //TOPICS
    public final static String TOPIC_CLIENTE_CREADO = "cliente-creado";
    public final static String TOPIC_CLIENTE_ACTUALIZADO = "cliente-actualizado";

    //GRUPOID
    public final static String CUENTA_GRUPO = "cuentaGroup";

    //MAP KEY CLIENTE
    public final static String CLIENTEID = "clienteId";
    public final static String NOMBRE = "nombre";
    
    //CUENTAS
    public final static String CUENTA_CREADA = "Cuenta creada para el cliente con ID: ";
    public final static String CUENTA_ACTUALIZADA = "Cuenta actualizada para el cliente con ID: ";
    public final static String DATOS_INCOMPLETOS_CREAR = "Datos de cliente incompletos, no se puede crear la cuenta.";
    public final static String DATOS_INCOMPLETOS_ACTUALIZAR = "Datos de cliente incompletos, no se puede actualizar la cuenta.";
    public final static String AHORROS = "Ahorros";
    public final static String CORRIENTE = "Corriente";

    //MOVIMIENTOS
    public final static String RETIRO = "retiro";
    public final static String DEPOSITO = "deposito";
    public final static String SALDO = "Saldo no disponible";





}
