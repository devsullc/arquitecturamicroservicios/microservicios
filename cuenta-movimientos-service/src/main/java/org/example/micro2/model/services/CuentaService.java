package org.example.micro2.model.services;

import org.example.micro2.model.dto.CuentaDTO;

public interface CuentaService {
    CuentaDTO crearCuenta(CuentaDTO cuentaDTO);
    CuentaDTO obtenerCuenta(Long id);
    CuentaDTO actualizarCuenta(Long id, CuentaDTO cuentaDTO);
    void eliminarCuenta(Long id);
}
