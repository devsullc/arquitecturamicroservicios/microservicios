package org.example.micro2.model.entitys;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;


@Data
@Entity
public class Movimiento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime fecha;
    private String tipoMovimiento;
    private BigDecimal valor;
    private BigDecimal saldoPostMovimiento;

    @ManyToOne
    @JoinColumn(name = "cuenta_id", nullable = false, referencedColumnName = "id")
    @ToString.Exclude
    @JsonBackReference
    private Cuenta cuenta;

}

