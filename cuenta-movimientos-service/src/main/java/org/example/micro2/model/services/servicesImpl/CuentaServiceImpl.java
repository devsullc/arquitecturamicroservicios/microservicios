package org.example.micro2.model.services.servicesImpl;

import org.example.micro2.constantes.Constantes;
import org.example.micro2.model.dto.CuentaDTO;
import org.example.micro2.model.entitys.Cuenta;
import org.example.micro2.model.repositorys.CuentaRepository;
import org.example.micro2.model.services.CuentaService;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.List;


@Service
public class CuentaServiceImpl implements CuentaService {

    @Autowired
    private CuentaRepository cuentaRepository;

    @Autowired
    private ModelMapper modelMapper;

    @KafkaListener(topics = Constantes.TOPIC_CLIENTE_CREADO, groupId = Constantes.CUENTA_GRUPO)
    public void handleClienteCreado(String message) {
        try {
            JSONObject json = new JSONObject(message);
            String clienteId = json.optString(Constantes.CLIENTEID, null);
            String nombreCliente = json.optString(Constantes.NOMBRE, null);

            if (clienteId != null && nombreCliente != null) {
                System.out.println(clienteId + " - " + nombreCliente);
                CuentaDTO nuevaCuenta = new CuentaDTO();
                nuevaCuenta.setClienteId(clienteId);
                nuevaCuenta.setNumeroCuenta(generarNumeroDeCuenta());
                nuevaCuenta.setTipoCuenta(Constantes.AHORROS);
                nuevaCuenta.setSaldoInicial(BigDecimal.ZERO);
                nuevaCuenta.setEstado(true);
                crearCuenta(nuevaCuenta);
                System.out.println(Constantes.CUENTA_CREADA + clienteId);
            } else {
                System.err.println(Constantes.DATOS_INCOMPLETOS_CREAR);
            }
        } catch (Exception e) {
            System.err.println(Constantes.ERROR_PROCESAR_MENSAJE + e.getMessage());
        }
    }
    @KafkaListener(topics = Constantes.TOPIC_CLIENTE_ACTUALIZADO, groupId = Constantes.CUENTA_GRUPO)
    public void handleClienteActualizado(String message) {
        try {
            JSONObject json = new JSONObject(message);
            String clienteId = json.optString(Constantes.CLIENTEID, null);
            String nombreCliente = json.optString(Constantes.NOMBRE, null);
            System.out.println(clienteId + " - "  + " - " + nombreCliente + " - " + clienteId);
            if (clienteId != null && nombreCliente != null) {
                System.out.println(clienteId + " - "  + nombreCliente + " - " + clienteId);
                CuentaDTO nuevaCuenta = new CuentaDTO();
                List<Cuenta> cuentaExistente = cuentaRepository.findByClienteId(clienteId);
                System.out.println(cuentaExistente.get(0).getId() + " - " + cuentaExistente.get(0).getNumeroCuenta());
                nuevaCuenta.setClienteId(clienteId);
                nuevaCuenta.setNumeroCuenta(cuentaExistente.get(0).getNumeroCuenta());
                nuevaCuenta.setTipoCuenta(cuentaExistente.get(0).getTipoCuenta());
                nuevaCuenta.setSaldoInicial(cuentaExistente.get(0).getSaldoInicial());
                nuevaCuenta.setEstado(cuentaExistente.get(0).getEstado());
                actualizarCuenta(cuentaExistente.get(0).getId(), nuevaCuenta);
                System.out.println(Constantes.CUENTA_ACTUALIZADA + clienteId);
            } else {
                System.err.println(Constantes.DATOS_INCOMPLETOS_ACTUALIZAR);
            }
        } catch (Exception e) {
            System.err.println(Constantes.ERROR_PROCESAR_MENSAJE + e.getMessage());
        }
    }




    private String generarNumeroDeCuenta() {
        return String.valueOf(System.currentTimeMillis());
    }

    @Override
    public CuentaDTO crearCuenta(CuentaDTO cuentaDTO) {
        Cuenta cuenta = modelMapper.map(cuentaDTO, Cuenta.class);
        cuenta.setNumeroCuenta(generarNumeroDeCuenta());
        cuenta = cuentaRepository.save(cuenta);
        return modelMapper.map(cuenta, CuentaDTO.class);
    }

    @Override
    public CuentaDTO obtenerCuenta(Long id) {
        Cuenta cuenta = cuentaRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(Constantes.ERROR_CUENTA_NO_ENCOTRADO));
        return modelMapper.map(cuenta, CuentaDTO.class);
    }

    @Override
    public CuentaDTO actualizarCuenta(Long id, CuentaDTO cuentaDTO) {
        Cuenta cuentaExistente = cuentaRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(Constantes.ERROR_CUENTA_NO_ENCOTRADO));
        cuentaExistente.setNumeroCuenta(cuentaDTO.getNumeroCuenta());
        cuentaExistente.setTipoCuenta(cuentaDTO.getTipoCuenta());
        cuentaExistente.setSaldoInicial(cuentaDTO.getSaldoInicial());
        cuentaExistente.setEstado(cuentaDTO.getEstado());
        cuentaExistente.setClienteId(cuentaDTO.getClienteId());
        cuentaExistente = cuentaRepository.save(cuentaExistente);
        return modelMapper.map(cuentaExistente, CuentaDTO.class);
    }

    @Override
    public void eliminarCuenta(Long id) {
        if (!cuentaRepository.existsById(id)) {
            throw new RuntimeException(Constantes.ERROR_CUENTA_NO_ENCOTRADO);
        }
        cuentaRepository.deleteById(id);
    }
}
