package org.example.micro2.model.repositorys;

import org.example.micro2.model.dto.CuentaDTO;
import org.example.micro2.model.entitys.Cuenta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CuentaRepository extends JpaRepository<Cuenta, Long> {
    List<Cuenta> findByClienteId(String clienteId);
}

