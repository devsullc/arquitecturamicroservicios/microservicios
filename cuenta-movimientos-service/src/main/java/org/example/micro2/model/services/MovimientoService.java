package org.example.micro2.model.services;

import org.example.micro2.exception.InsufficientFundsException;
import org.example.micro2.model.dto.MovimientoDTO;

public interface MovimientoService {
    MovimientoDTO createMovimiento(MovimientoDTO movimientoDTO) throws InsufficientFundsException;
    MovimientoDTO getMovimientoById(Long id);
}

