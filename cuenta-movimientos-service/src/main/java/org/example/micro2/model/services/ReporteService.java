package org.example.micro2.model.services;

import org.example.micro2.model.dto.ReporteEstadoCuentaDTO;

import java.time.LocalDateTime;

public interface ReporteService {

    ReporteEstadoCuentaDTO generarReporteEstadoDeCuenta(LocalDateTime fechaInicio, LocalDateTime fechaFin, String clienteId);
}

