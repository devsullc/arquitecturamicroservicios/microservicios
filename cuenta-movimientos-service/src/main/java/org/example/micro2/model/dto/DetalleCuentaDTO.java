package org.example.micro2.model.dto;

import lombok.Data;
import org.example.micro2.model.entitys.Movimiento;

import java.math.BigDecimal;
import java.util.List;

@Data
public class DetalleCuentaDTO {
    private String numeroCuenta;
    private String tipoCuenta;
    private BigDecimal saldoActual;
    private List<Movimiento> movimientos;
}
