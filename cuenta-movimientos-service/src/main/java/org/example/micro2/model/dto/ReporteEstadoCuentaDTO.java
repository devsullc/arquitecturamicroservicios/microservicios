package org.example.micro2.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class ReporteEstadoCuentaDTO {
    private String clienteId;
    private List<DetalleCuentaDTO> cuentas;
}