package org.example.micro2.model.services.servicesImpl;

import org.example.micro2.constantes.Constantes;
import org.example.micro2.exception.InsufficientFundsException;
import org.example.micro2.model.dto.MovimientoDTO;
import org.example.micro2.model.entitys.Cuenta;
import org.example.micro2.model.entitys.Movimiento;
import org.example.micro2.model.repositorys.CuentaRepository;
import org.example.micro2.model.repositorys.MovimientoRepository;
import org.example.micro2.model.services.MovimientoService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

@Service
public class MovimientoServiceImpl implements MovimientoService {

    @Autowired
    private MovimientoRepository movimientoRepository;

    @Autowired
    private CuentaRepository cuentaRepository;

    @Autowired
    private ModelMapper modelMapper;


    @Override
    public MovimientoDTO createMovimiento(MovimientoDTO movimientoDTO) throws InsufficientFundsException {
        Cuenta cuenta = cuentaRepository.findById(Long.valueOf(movimientoDTO.getCuentaId()))
                .orElseThrow(() -> new IllegalArgumentException(Constantes.ERROR_CUENTA_NO_ENCOTRADO + movimientoDTO.getCuentaId()));

        if (Constantes.RETIRO.equals(movimientoDTO.getTipoMovimiento()) && cuenta.getSaldoInicial().compareTo(movimientoDTO.getValor()) < 0) {
            throw new InsufficientFundsException(Constantes.SALDO);
        }

        cuenta.setSaldoInicial(cuenta.getSaldoInicial().add(movimientoDTO.getTipoMovimiento().equals(Constantes.DEPOSITO) ? movimientoDTO.getValor() : movimientoDTO.getValor().negate()));
        cuentaRepository.save(cuenta);

        Movimiento movimiento = modelMapper.map(movimientoDTO, Movimiento.class);
        movimiento.setCuenta(cuenta);
        movimiento.setFecha(LocalDateTime.now());
        movimiento.setSaldoPostMovimiento(cuenta.getSaldoInicial());

        Movimiento savedMovimiento = movimientoRepository.save(movimiento);
        return modelMapper.map(savedMovimiento, MovimientoDTO.class);
    }

    @Override
    public MovimientoDTO getMovimientoById(Long id) {
        Movimiento movimiento = movimientoRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(Constantes.ERROR_MOVIMIENTOS_NO_ENCOTRADO + id));
        return modelMapper.map(movimiento, MovimientoDTO.class);
    }
}


