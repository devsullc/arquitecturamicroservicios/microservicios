package org.example.micro2.model.services.servicesImpl;

import org.example.micro2.model.dto.DetalleCuentaDTO;
import org.example.micro2.model.dto.ReporteEstadoCuentaDTO;
import org.example.micro2.model.entitys.Cuenta;
import org.example.micro2.model.entitys.Movimiento;
import org.example.micro2.model.repositorys.CuentaRepository;
import org.example.micro2.model.repositorys.MovimientoRepository;
import org.example.micro2.model.services.ReporteService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReporteServiceImpl implements ReporteService {

    private final CuentaRepository cuentaRepository;
    private final MovimientoRepository movimientoRepository;

    public ReporteServiceImpl(CuentaRepository cuentaRepository, MovimientoRepository movimientoRepository) {
        this.cuentaRepository = cuentaRepository;
        this.movimientoRepository = movimientoRepository;
    }

    @Override
    public ReporteEstadoCuentaDTO generarReporteEstadoDeCuenta(LocalDateTime fechaInicio, LocalDateTime fechaFin, String clienteId) {
        List<Cuenta> cuentas = cuentaRepository.findByClienteId(clienteId);
        List<DetalleCuentaDTO> detalles = new ArrayList<>();

        for (Cuenta cuenta : cuentas) {
            List<Movimiento> movimientos = movimientoRepository.findByCuentaIdAndFechaBetween(cuenta.getId(), fechaInicio, fechaFin);
            DetalleCuentaDTO detalle = new DetalleCuentaDTO();
            detalle.setNumeroCuenta(cuenta.getNumeroCuenta());
            detalle.setTipoCuenta(cuenta.getTipoCuenta());
            detalle.setSaldoActual(cuenta.getSaldoInicial().add(movimientos.stream().map(Movimiento::getValor).reduce(BigDecimal.ZERO, BigDecimal::add)));
            detalle.setMovimientos(movimientos);
            detalles.add(detalle);
        }

        ReporteEstadoCuentaDTO reporte = new ReporteEstadoCuentaDTO();
        reporte.setClienteId(clienteId);
        reporte.setCuentas(detalles);

        return reporte;
    }
}
