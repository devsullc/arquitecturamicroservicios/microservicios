package org.example.micro2.controller;

import org.example.micro2.constantes.Constantes;
import org.example.micro2.exception.InsufficientFundsException;
import org.example.micro2.model.dto.ReporteEstadoCuentaDTO;
import org.example.micro2.model.services.ReporteService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;


@RestController
@RequestMapping("/reportes")
public class ReporteController {

    private final ReporteService reporteService;

    public ReporteController(ReporteService reporteService) {
        this.reporteService = reporteService;
    }

    @GetMapping()
    public ResponseEntity<ReporteEstadoCuentaDTO> obtenerReporteDeEstadoDeCuenta(
            @RequestParam("fechaInicio") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaInicio,
            @RequestParam("fechaFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaFin,
            @RequestParam("clienteId") String clienteId) {

        if (fechaInicio == null || fechaFin == null || clienteId == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constantes.ERROR_FECHAS_CLIENTE );
        }

        if (fechaFin.isBefore(fechaInicio)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constantes.ERROR_FECHAS);
        }

        LocalDateTime inicioDateTime = fechaInicio.atStartOfDay();
        LocalDateTime finDateTime = fechaFin.atTime(LocalTime.MAX);

        try {
            ReporteEstadoCuentaDTO reporte = reporteService.generarReporteEstadoDeCuenta(inicioDateTime, finDateTime, clienteId);
            return ResponseEntity.ok(reporte);
        } catch (InsufficientFundsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constantes.ERROR_CLIENTE_NO_ENCOTRADO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

}

