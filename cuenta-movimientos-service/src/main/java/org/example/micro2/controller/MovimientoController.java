package org.example.micro2.controller;

import org.example.micro2.constantes.Constantes;
import org.example.micro2.exception.InsufficientFundsException;
import org.example.micro2.model.dto.MovimientoDTO;
import org.example.micro2.model.services.MovimientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/movimientos")
public class MovimientoController {
    @Autowired
    private MovimientoService movimientoService;

    @PostMapping
    public ResponseEntity<?> registrarMovimiento(@RequestBody MovimientoDTO movimientoDTO) {
        try {
            MovimientoDTO nuevoMovimiento = movimientoService.createMovimiento(movimientoDTO);
            return new ResponseEntity<>(nuevoMovimiento, HttpStatus.CREATED);
        } catch (InsufficientFundsException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Constantes.ERROR_SERVER);
        }
    }


    @GetMapping("/{id}")
    public ResponseEntity<MovimientoDTO> obtenerMovimiento(@PathVariable Long id) {
        try {
            MovimientoDTO movimiento = movimientoService.getMovimientoById(id);
            return new ResponseEntity<>(movimiento, HttpStatus.OK);
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }
}

